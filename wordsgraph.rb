str=[]
noun=[]
list=[]
graph=[]

#nounnum=[]

while line = gets
    str<<line.chomp
    noun<<line.chomp
end

str.select! do |line|
    line=~/名詞/
end
str.map! do |line|
    line.gsub!(/\t.*/,"")
end

noun.select! do |line|
    line=~/名詞/ or line=~/。/
end
noun.map! do |line|
    line.gsub!(/\t.*/,"")
end

file=File.open("noun.txt","w")
noun.each do |e|
    file.puts(e)
end
file.flush

list=str.uniq!

#file=File.open("list.txt","w")
#list.each do |e|
#    file.puts(e)
#end
#file.flush

#puts "list.size=#{list.size}"
#puts "noun.size=#{noun.select {|line| line=~/[^。]/}.size}"

noun.each_with_index do |before,i|
    break if i+1==noun.size
    after=noun[i+1]
    if after=~/[^。]/ and before=~/[^。]/ 
        graph[list.index(before)]||= {}
        if graph[list.index(before)][after]
            graph[list.index(before)][after]+=1
        else
            graph[list.index(before)][after]=1
        end
    end
end

data=[]
graph.each_with_index do |h,i|
    next unless h
    before=list[i]
    h.keys.each do |k|
        data << [before,k,h[k]]
    end
end

data.sort_by!{|a| a[2] }.reverse!

file=File.open("wordsgraph.csv","w")
data.each do |line|
    file.puts("#{line[0]},#{line[1]},#{line[2]}")
end
file.flush




#puts noun
#puts list