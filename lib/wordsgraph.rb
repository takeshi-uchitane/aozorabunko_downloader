def wordsgraph(filename,basedir)

str=[]
noun=[]
list=[]
graph=[]

file=open(basedir+"/"+filename)
file.each do |line|
    str<<line.chomp
    noun<<line.chomp
end

str.select! do |line|
    line=~/����/
end
str.map! do |line|
    line.gsub!(/\t.*/,"")
end

noun.select! do |line|
    line=~/����/ or line=~/�B/
end
noun.map! do |line|
    line.gsub!(/\t.*/,"")
end

file=File.open("#{basedir}/noun.txt","w")
noun.each do |e|
    file.puts(e)
end
file.flush

list=str.uniq!

file=File.open("#{basedir}/list.txt","w")
list.each do |e|
    file.puts(e)
end
file.flush

noun.each_with_index do |before,i|
    break if i+1==noun.size
    after=noun[i+1]
    if after=~/[^�B]/ and before=~/[^�B]/ 
        graph[list.index(before)]||= {}
        if graph[list.index(before)][after]
            graph[list.index(before)][after]+=1
        else
            graph[list.index(before)][after]=1
        end
    end
end

data=[]
graph.each_with_index do |h,i|
    next unless h
    before=list[i]
    h.keys.each do |k|
        data << [before,k,h[k]]
    end
end

data.sort_by!{|a| a[2] }.reverse!

file=open("#{basedir}/wordsgraph.csv","w+")
data.each do |line|
    file.puts("#{line[0]},#{line[1]},#{line[2]}")
end
file.flush

end