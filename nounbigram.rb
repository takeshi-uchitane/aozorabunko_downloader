require 'zip'
require_relative 'lib/delruby'
require_relative 'lib/wordsgraph'

filelist=[]
basedir=File.dirname(ARGV[0])
File.open(ARGV[0]).each do |line|
    filelist<<line.chomp
end

filelist.each do |file|
    filename=nil
    Zip::File.open("bookshelf/"+file) do |zip|
        zip.each do |entry|
            puts "entry #{entry.to_s}"
            zip.extract(entry, basedir+"/"+entry.to_s) { true }
            filename=entry.to_s
        end
    end
delruby(filename,basedir)
cmd="mecab #{basedir}/rubyfree_#{filename} > #{basedir}/mecab_#{filename}"
system(cmd)
cmd2="nkf32 -s --overwrite #{basedir}/mecab_#{filename}"
system(cmd2)

file2=open("#{basedir}/mecab_#{filename}")
file3=open("#{basedir}/mecab_joint.txt","a")
file3.write(file2.read())
file3.flush
end

wordsgraph("mecab_joint.txt",basedir)

rscript=<<-EOS
library(igraph)
wg<-read.csv("wordsgraph.csv", head=F)
wg2 <-wg[wg$V3>DEGREE,]
g<-graph.data.frame(wg2)
png("graph.png",width=600,height=600)
plot.igraph(g)
dev.off()
EOS

degree=1

rscript.gsub!(/DEGREE/,"#{degree}")
file=open("#{basedir}/rscript.R",'w')
    file.puts(rscript)
file.flush

Dir.chdir(basedir)
cmd="Rscript --vanilla rscript.R"
system(cmd)
