require 'open-uri'
require 'nokogiri'

STDOUT.sync = true 
output="urllist.txt"
urllist=[]

require_relative "lib/list_reader"

book_list = list_read

url_base = "http://www.aozora.gr.jp/cards/"

book_list.each do |list|
url = url_base + list + ".html"

begin
    charset = nil
    html = open(url) do |f|
        charset = f.charset
        f.read
    end

    doc = Nokogiri::HTML.parse(html, nil, charset)
    doc.xpath('//table[@class="download"]//a').each do |atag|
	if (atag.children[0].to_s=~/zip/)!=nil then
		url=url.gsub(/cards/,"spare")
		author=url.gsub(/card.*html/,"")
		author=author.gsub(/spare/,"cards")
		fullurl=author+"files/"+atag.children[0].to_s
		puts fullurl
		urllist<<fullurl
	end
    end
rescue
    next
end
end

io=open(output,"w")
urllist.each do |line|
    io.puts line
end
io.flush
io.close